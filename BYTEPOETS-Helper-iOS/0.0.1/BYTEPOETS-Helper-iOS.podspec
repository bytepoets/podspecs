Pod::Spec.new do |s|
  s.name         = "BYTEPOETS-Helper-iOS"
  s.version      = "0.0.1"
  s.summary      = "A collection of helper methods/classes/categories"
  s.homepage     = 'http://www.bytepoets.com'
  s.platform     = :ios
  s.license      = { :type => 'MIT', :text => '' }
  s.author       = { "BYTEPOETS" => "office@bytepoets.com" }

  s.source_files = "**/*.{h,m}"

  s.source       = {
    :git => "ssh://git@dev.bytepoets.com/~git/bytepoets-helper-ios.git",
    :tag => "0.0.1"
  }

end
