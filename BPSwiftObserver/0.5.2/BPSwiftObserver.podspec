#
# Be sure to run `pod lib lint SwiftObserver.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BPSwiftObserver'
  s.version          = '0.5.2'
  s.summary          = 'Observer pattern in Swift'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This pod implements the observer pattern for swift
                       DESC

  s.homepage         = 'https://bitbucket.org/bytepoets/bpswiftobserver'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Martin Eberl' => 'martin.eberl@bytepoets.com' }
  s.source           = { :git => 'https://bytepoets-meb@bitbucket.org/bytepoets/bpswiftobserver.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

s.source_files = 'SwiftObserver/Classes/**/*.{swift}'

  # s.resource_bundles = {
  #   'BPSwiftObserver' => ['SwiftObserver/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
