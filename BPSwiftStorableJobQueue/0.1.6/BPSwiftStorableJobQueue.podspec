#
# Be sure to run `pod lib lint SwiftStorableJobQueue.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BPSwiftStorableJobQueue'
  s.version          = '0.1.6'
  s.summary          = 'The queue stores jobs, that may later be started asynchronousely'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This is a queue manager, that can store jobs and call them asynchronously.
                       DESC

  s.homepage         = 'https://bitbucket.org/bytepoets/bpswiftstorablejobqueue'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Martin Eberl' => 'martin.eberl@bytepoets.com' }
  s.source           = { :git => 'https://bytepoets-meb@bitbucket.org/bytepoets/bpswiftstorablejobqueue.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'SwiftStorableJobQueue/Classes/**/*.{swift}'

  # s.resource_bundles = {
  #   'BPSwiftStorableJobQueue' => ['SwiftStorableJobQueue/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'Foundation'
  s.dependency 'BPSwiftObserver'

end
