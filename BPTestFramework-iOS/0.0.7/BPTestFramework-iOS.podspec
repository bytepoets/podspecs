Pod::Spec.new do |s|
  s.name         = "BPTestFramework-iOS"
  s.version      = "0.0.7"
  s.summary      = "A collection of commands to make testing easier."
  s.homepage     = 'http://www.bytepoets.com'
  s.platform     = :ios
  s.license      = { :type => 'MIT', :text => '' }
  s.author       = { "BYTEPOETS" => "office@bytepoets.com" }
  
  s.preserve_paths = "**"
  
  s.source       = {
    :git => "ssh://git@dev.bytepoets.com/~git/testingframework-ios.git", 
    :tag => "0.0.4"
  }

  s.xcconfig = { 'OTHER_LDFLAGS' => '$(XCODE_TEST_LDFLAGS)' }
end
