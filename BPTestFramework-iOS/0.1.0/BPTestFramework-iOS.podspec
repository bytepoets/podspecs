Pod::Spec.new do |s|
  s.name         = "BPTestFramework-iOS"
  s.version      = "0.1.0"
  s.summary      = "A collection of commands to make testing easier."
  s.homepage     = 'http://www.bytepoets.com'
  s.platform     = :ios
  s.license      = { :type => 'MIT', :text => '' }
  s.author       = { "BYTEPOETS" => "office@bytepoets.com" }

  s.preserve_paths = "**"

  s.source       = {
    :git => "ssh://git@bitbucket.org/bytepoets/testingframework-ios.git",
    :tag => "0.0.6"
  }

  s.xcconfig = { 'OTHER_LDFLAGS' => '$(XCODE_TEST_LDFLAGS)' }
end
