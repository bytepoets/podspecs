Pod::Spec.new do |s|
  s.name         = "BPTestFramework-iOS"
  s.version      = "0.0.2"
  s.summary      = "A collection of commands to make enabling of testing easier."
  s.homepage     = 'http://www.bytepoets.com'
  s.platform     = :ios
  s.license      = { :type => 'MIT', :text => '' }
  s.author       = { "BYTEPOETS" => "office@bytepoets.com" }
  s.resources    = "**/*.*"

  s.source       = {
    :git => "ssh://git@dev.bytepoets.com/~git/testingframework-ios.git", 
    :tag => "0.0.2"
  }
end
